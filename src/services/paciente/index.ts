const axios = require('axios').default;

const pacienteCadastrar = async () => {
    try {
        const resp = await axios.get('http://localhost:8081/paciente-api/pacientes/cadastrar');
        console.log(resp.data);
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
};

export default {pacienteCadastrar};
    
    