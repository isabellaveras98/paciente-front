import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import axios from 'axios';

import { UserData } from '../../providers/user-data';
// import PacienteService  from '../../../services/paciente'

import { UserOptions } from '../../interfaces/user-options';



@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
  styleUrls: ['./signup.scss'],
})
export class SignupPage {
  signup: UserOptions = { login: '', senha: '', email: ''};
  submitted = false;

  constructor(
    public router: Router,
    public userData: UserData
  ) {}

  onSignup(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.userData.signup(this.signup.login);
      this.router.navigateByUrl('/app/tabs/schedule');
    }
  }

  async cadastrar(form: NgForm){
    try {
      if (form.valid) {
        const resp = await axios.get('http://localhost:8081/paciente-api/pacientes/cadastrar',{
          headers: {
            login: this.signup.login,
            senha: this.signup.senha,
            email: this.signup.email
          }
        });
        console.log(resp.data);
      }
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
  }
  
}
